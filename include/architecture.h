#ifndef ARCHITECTURE_H
#define ARCHITECTURE_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include <inttypes.h>
#include <SFML/Graphics.h>

#include "math2d.h"

#define N 256

typedef uint32_t u32;
typedef uint64_t u64;
typedef int32_t i32;
typedef int64_t i64;
typedef float f32;
typedef double f64;

typedef struct { u32 gene, inde; } ID;

#define FIELDS \
	FIELD(state, struct State) \
	FIELD(sprite, sfSprite*) \
	FIELD(velocity, sfVector2f) \
	FIELD(acceleration, sfVector2f)

#define FIELD(field, type) type *get_##field(u32 id);
	FIELDS
#undef FIELD

#define iter_entities(item) \
	for(uint32_t item=get_last_entity(); item != -1; next_entity(&item))

void add_forces(sfVector2f acceleration);

u32 get_last_entity();
void next_entity(u32 *it);

void debug_entity_data(u32 id);

void init_architecture();
void drop_architecture();

u32 spawn_entity(sfVector2f position, sfIntRect rect);
bool despawn_entity(u32 id);

bool is_entity_active(u32 id);

void update_system();
void render_system();

#endif
