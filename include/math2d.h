#ifndef MATH2D_H
#define MATH2D_H

#include <SFML/System/Vector2.h>
#include <math.h>

#define FN_OP\
	X(vec2, sfVector2f, sfVector2f, sfVector2f, add, a, b, { return ((sfVector2f) { a.x+b.x, a.y+b.y }); }) \
	X(vec2, sfVector2f, sfVector2f, sfVector2f, sub, a, b, { return ((sfVector2f) { a.x-b.x, a.y-b.y }); }) \
	X(vec2, sfVector2f, sfVector2f, sfVector2f, mul, a, b, { return ((sfVector2f) { a.x*b.x, a.y*b.y }); }) \
	X(vec2, sfVector2f, sfVector2f, sfVector2f, div, a, b, { return ((sfVector2f) { a.x/b.x, a.y/b.y }); }) \
	X(vec2, sfVector2f, sfVector2f, float, scale_add, a, b, { return ((sfVector2f) { a.x+b, a.y+b }); }) \
	X(vec2, sfVector2f, sfVector2f, float, scale_mul, a, b, { return ((sfVector2f) { a.x*b, a.y*b }); }) \
	X(vec2, sfVector2f, sfVector2f, float, scale_div, a, b, { return ((sfVector2f) { a.x/b, a.y/b }); })

#define X(name_mod, type_ret, type_a, type_b, name_op, name_a, name_b, ...) \
type_ret name_mod##_##name_op(type_a name_a, type_b name_b); \
void name_mod##_##name_op##_assign(type_a *name_a, type_b name_b);
	FN_OP
#undef X

float vec2_sq_length(sfVector2f v);
float vec2_length(sfVector2f v);
sfVector2f vec2_normalized(sfVector2f v);

#endif
