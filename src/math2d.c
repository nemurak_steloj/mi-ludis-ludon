#include "math2d.h"

#define X(name_mod, type_ret, type_a, type_b, name_op, name_a, name_b, body)\
type_ret name_mod##_##name_op(type_a name_a, type_b name_b) body \
void name_mod##_##name_op##_assign(type_a *name_a, type_b name_b) { *a = name_mod##_##name_op(*a, b); }
	FN_OP
#undef X

float vec2_sq_length(sfVector2f v) { return v.x*v.x+v.y*v.y; }
float vec2_length(sfVector2f v) { return sqrtf(vec2_sq_length(v)); }
sfVector2f vec2_normalized(sfVector2f v) {
	float l = sqrt(v.x*v.x+v.y*v.y);
	return (sfVector2f) { v.x/l, v.y/l };
}

