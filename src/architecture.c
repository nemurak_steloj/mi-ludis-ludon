#include "architecture.h"

sfTexture *texture = NULL;
extern sfRenderWindow *window;
extern sfVector2f size;
sfVector2f g_acceleration = {0};

void add_forces(sfVector2f acceleration) {
	vec2_add_assign(&g_acceleration, acceleration);
}

struct State {
	u32 prev, next;

	u32 gene: 31;
	union { bool value, active; };
};

struct ID { u32 inde, gene; };

struct {
#define FIELD(field, type) type* field;
	FIELDS
#undef FIELD
	uint64_t
		header_acti,
		header_inac,
		len,
		dim;
} static entity_register = { 0, .dim = N };

#define FIELD(field, type) \
type *get_##field(u32 id) { return &entity_register.field[id]; }
	FIELDS
#undef FIELD

u32 get_last_entity() {
	return entity_register.header_acti - 1;
}

void next_entity(u32 *id) {
	*id = entity_register.state[*id].next-1;
}

void debug_entity_data(uint32_t id) {
	printf("id = %03d, next = %03d, prev = %03d, acti = %03d, acti_h = %03ld, inac_h = %03ld\n",
		id+1,
		entity_register.state[id].next,
		entity_register.state[id].prev,
		entity_register.state[id].active,
		entity_register.header_acti,
		entity_register.header_inac
	);
}

#define heap_put(heap, header, i, v) \
	typeof(heap) node = &heap[i]; \
	if (header != 0) { \
		heap[header-1].prev = i+1; \
		node->next = header; \
		node->prev = 0; \
	} else node->next = 0; \
	header = i+1; \
	node->value = v; \

#define heap_pop(heap, header, i) \
	typeof(heap) node = &heap[i]; \
	if (header-1 == i) header = node->next; \
	if (node->prev != 0) heap[node->prev-1].next = node->next; \
	if (node->next != 0) heap[node->next-1].prev = node->prev; \

void state_active_pop(u32 id) {
	{ heap_pop(entity_register.state, entity_register.header_acti, id) }
	{ heap_put(entity_register.state, entity_register.header_inac, id, false) }
}

void state_active_put(u32 id) {
	struct State *heap = entity_register.state;
	{ heap_pop(entity_register.state, entity_register.header_inac, id) }
	{ heap_put(entity_register.state, entity_register.header_acti, id, true) }
}

void init_architecture() {
	texture = sfTexture_createFromFile("./assets/touhou_pixelart.png", NULL);
#define FIELD(field, type) entity_register.field = calloc(sizeof(type), N);
	FIELDS
#undef FIELD
}

void drop_architecture() {
	sfTexture_destroy(texture);
#define FIELD(field, ...) free(entity_register.field);
	FIELDS
#undef FIELD
}

u32 spawn_entity(sfVector2f position, sfIntRect rect) {
	if (entity_register.len >= entity_register.dim) {
		printf("resized\n");
		entity_register.dim *= 2;
		#define FIELD(field, type) entity_register.field = realloc(entity_register.field, entity_register.dim*sizeof(type));
			FIELDS
		#undef FIELD
	}
	// Assign ID
	uint32_t id;
	if (entity_register.header_inac != 0) {
		id = entity_register.header_inac - 1;
		entity_register.header_inac = entity_register.state[id].next;
	} else {
		id = entity_register.len++;
		entity_register.state[id] = (struct State) {0};
		entity_register.sprite[id] = NULL;
		entity_register.velocity[id] =  (sfVector2f) {0};
		entity_register.acceleration[id] =  (sfVector2f) {0};
	}

	state_active_put(id);

	// Initialize data
	sfSprite *sprite = sfSprite_create();
	{
		sfSprite_setTexture(sprite, texture, sfTrue);
		sfSprite_setTextureRect(sprite, rect);
		sfSprite_setPosition(sprite, position);
		sfSprite_setOrigin(sprite, (sfVector2f) { 18, 18 });
		sfSprite_scale(sprite, (sfVector2f) { 2., 2. });
	}
	entity_register.sprite[id] = sprite;

	float length = (float)rand()/RAND_MAX * 10.0;
	entity_register.velocity[id] = (sfVector2f) {
		.x = (float)rand()/(float)RAND_MAX*length,
		.y = (float)rand()/(float)RAND_MAX*length
	};

	return id;
}

bool despawn_entity(u32 id) {
	struct State *node = &entity_register.state[id];
	if (node->value == false)
		return false;

	state_active_pop(id);
	node->gene++;

	// Clear data
	sfSprite_destroy(entity_register.sprite[id]);
	entity_register.sprite[id] = NULL;
	entity_register.velocity[id] = (sfVector2f){0};
	entity_register.acceleration[id] = (sfVector2f){0};

	return true;
}

bool is_entity_active(u32 id) {
	return entity_register.state[id].active;
}

void update_system() {
	iter_entities(id) {
		sfVector2f *acceleration = get_acceleration(id);
		sfVector2f *velocity = get_velocity(id);
		sfSprite *sprite = *(sfSprite**)get_sprite(id);

		vec2_add_assign(acceleration, g_acceleration);

		float sq_length = vec2_sq_length(*velocity);
		if (sq_length > 0) {
			float lenght = sqrtf(sq_length);
			float coefficient = 0.001;
			float drag_magnitud = coefficient*lenght;
			sfVector2f drag = vec2_scale_mul(*velocity, drag_magnitud/lenght);
			vec2_sub_assign(acceleration, drag);
		}

		vec2_add_assign(velocity, *acceleration);
		sfSprite_move(sprite, *velocity);
		*acceleration = (sfVector2f) {0};

		sfVector2f scale = sfSprite_getScale(sprite);
		if (velocity->x > 0 && scale.x > 0) {
			scale.x *= -1;
			sfSprite_setScale(sprite, scale);
		} else if (velocity->x < 0 && scale.x < 0) {
			scale.x *= -1;
			sfSprite_setScale(sprite, scale);
		}

		sfVector2f position = sfSprite_getPosition(sprite);
		{
			if (position.x > size.x - 36) {
				position.x = size.x - 36;
				velocity->x *= -1.0;
			} else if (position.x < 36) {
				position.x = 36;
				velocity->x *= -1.0;
			}

			if (position.y > size.y - 36) {
				position.y = size.y - 36;
				velocity->y *= -1.0;
			}/* else if (position.y < 36) {
				position.y = 36;
				velocity->y *= -1.0;
			}*/
		}
		sfSprite_setPosition(sprite, position);
	}
	g_acceleration = (sfVector2f) {0};
}

void render_system() {
	sfRenderWindow_clear(window, sfBlack);
	iter_entities(id) sfRenderWindow_drawSprite(window, entity_register.sprite[id], NULL);
	sfRenderWindow_display(window);
}
