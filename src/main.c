#include "architecture.h"

sfRenderWindow *window = NULL;
sfVector2f size = { 1000, 500 };
sfEvent event;

void move_player(int id) {
	if (!is_entity_active(id)) return;

	sfVector2f delta = {0, 0};
	if (sfKeyboard_isKeyPressed(sfKeyW)) delta.y -= 1.5;
	if (sfKeyboard_isKeyPressed(sfKeyS)) delta.y += 1.;
	if (sfKeyboard_isKeyPressed(sfKeyA)) delta.x -= 1.;
	if (sfKeyboard_isKeyPressed(sfKeyD)) delta.x += 1.;
	if (memcmp(&delta, &(sfVector2f){0, 0}, sizeof(sfVector2f))) {
		sfVector2f* acceleration = get_acceleration(id);
		//vec2_scale_div_assign(&delta, sqrtf(delta.x*delta.x + delta.y*delta.y));
		vec2_scale_mul_assign(&delta, 1.5);
		vec2_add_assign(acceleration, delta);
	}
}

sfVector2f wind = { 0.1, 0.0 };
sfVector2f gravity = { 0.0, 1.0 };

void forces() {
	add_forces(wind);
	add_forces(gravity);
}

int main() {
	srand(time(NULL));
	wind.x = (f32)rand()/RAND_MAX * 0.02 - 0.01;
	gravity.y = (f32)rand()/RAND_MAX*2.0 + 0.5;

	window = sfRenderWindow_create(
		(sfVideoMode) {
			.width = size.x,
			.height = size.y,
			.bitsPerPixel = 32
		},
		"SFML",
		sfResize | sfClose,
		NULL
	);
	sfRenderWindow_setFramerateLimit(window, 60);

	init_architecture();

	int player_id = spawn_entity(
		(sfVector2f) {0},
		(sfIntRect) {0, 0, 36, 36}
	);

	for (int i = 0; i < N*10 ; i++) {
		int32_t id = spawn_entity(
			(sfVector2f) {
				rand() % (int)size.x,
				rand() % (int)size.y,
			},
			(sfIntRect) {
				.left   = 36*(rand()%8  ),
				.top    = 36*(rand()%4+1),
				.width  = 36,
				.height = 36,
			}
		);
	}

	printf("\n");
	iter_entities(id) {
		debug_entity_data(id);
	}
	printf("\n");

	sfClock* clock = sfClock_create();
	while (sfRenderWindow_isOpen(window)) {
		while (sfRenderWindow_pollEvent(window, &event)) {
			if (event.type == sfEvtClosed)
				sfRenderWindow_close(window);
			if (event.type == sfEvtKeyPressed) {
				if (event.key.code == sfKeyEscape) sfRenderWindow_close(window);
				if (event.key.code == sfKeyR) {
					iter_entities(id) {
						sfVector2f *velocity = get_velocity(id);
						sfSprite *sprite = *get_sprite(id);
						vec2_scale_add_assign(velocity, ((float)rand()/RAND_MAX*2.0-1.0) * 10);
						sfSprite_setPosition(sprite, (sfVector2f) {
							rand() % (int)size.x,
							rand() % (int)size.y
						});
					}
				}
				if (event.key.code == sfKeySubtract) {
					float p = 0.25;
					i32 id = -1;
					u32 try = 0;
					while (id < 0 && try < 100000) {
						iter_entities(i) {
							float r = (f32)rand()/RAND_MAX;
							if (r <= p) { id = i; break; }
						}
						try++;
					}
					despawn_entity(id);
				}
				if (event.key.code == sfKeyAdd) {
					int32_t id = spawn_entity(
						(sfVector2f) {
							rand() % (int)size.x,
							rand() % (int)size.y,
						},
						(sfIntRect) {
							.left   = 36*(rand()%8  ),
							.top    = 36*(rand()%4+1),
							.width  = 36,
							.height = 36,
						}
					);
					if (id < 0) printf("Error::MAXCAP\n"); else debug_entity_data(id);
				}
			}
			if (event.type == sfEvtResized) {
				size = (sfVector2f) { event.size.width, event.size.height };
				sfFloatRect visibleArea = {0, 0, event.size.width, event.size.height};
				sfRenderWindow_setView(window, sfView_createFromRect(visibleArea));
			}
		}

		float dt = sfTime_asSeconds(sfClock_getElapsedTime(clock));
		if (dt > 1.0/60.0) {
			move_player(player_id);
			forces();
			update_system();
			render_system();
			sfClock_restart(clock);
		}
	}

	sfClock_destroy(clock);
	sfRenderWindow_destroy(window);
	drop_architecture();
	return 0;
}
